__author__ = 'estudiante'
import sys
import numpy


def leer():
    catalogo = open("./catalog", "r")

    vector1 = []
    vector2 = []
    for linea in catalogo.readlines():

        try:
            brillo = float(linea[103:107])
            vector1.append(brillo)
            codigo = float(linea[0:4])
            vector2.append(codigo)
        except:
            Exception()

    return numpy.array(vector1)



if len(sys.argv) < 2:
    print ("Debe indicar las magnitudes visuales mayores y menores que desea obtener.")
else:
    rango1 = float(sys.argv[1])
    rango2 = float(sys.argv[2])
    resultado = leer()
    modificado = resultado[(resultado > rango1) & (resultado < rango2)]


    modificado = numpy.sort(modificado)
    print modificado
    i=0
    while i < len(modificado):
        "Las magnitudes pedidas son:"
        print modificado[i]
        i+=1

exit()